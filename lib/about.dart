import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Tentang Kami",
        ),
      ),
      body: Container(
        color: Color.fromARGB(255, 87, 162, 243),
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu duis turpis venenatis at quis eget neque gravida nunc. Condimentum elit turpis urna, risus facilisis viverra imperdiet. Sagittis bibendum at turpis egestas diam nec. consectur adpisciting elit.Lorem ipsum dolor sit amet, ",
                    style: GoogleFonts.montserrat(
                        fontSize: 15.0, color: Colors.black),
                    textAlign: TextAlign.justify,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    "Official Partner",
                    style: GoogleFonts.montserrat(
                        fontSize: 15.0, color: Colors.black),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 2, right: 16),
              child: Row(
                children: [
                  Text(
                    "Kementrian Kesehatan",
                    style: GoogleFonts.montserrat(
                        fontSize: 15.0, color: Colors.black),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 2, right: 2),
              child: Row(
                children: [
                  Text(
                    "Kementrian Lingkungan Hidup Dan Perhutanan",
                    style: GoogleFonts.montserrat(
                        fontSize: 15.0, color: Colors.black),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
