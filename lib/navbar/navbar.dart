import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:login_flutter/controller/controller.dart';
import 'package:login_flutter/home.dart';
import 'package:login_flutter/profile.dart';
import 'package:login_flutter/setting.dart';

class Navbar extends StatefulWidget {
  const Navbar({super.key});

  @override
  State<Navbar> createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  final controller = Get.put(NavbarController());
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavbarController>(builder: (context) {
      return Scaffold(
        body: IndexedStack(
          index: controller.tabIndex,
          children: const [
            HomePage(),
            SettingPage(),
            ProfilePage(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: controller.tabIndex,
            onTap: controller.changeTabIndex,
            items: [
              _bottombaritem(IconlyBold.home, "Home"),
              _bottombaritem(IconlyBold.setting, "Setting"),
              _bottombaritem(IconlyBold.profile, "profile")
            ]),
      );
    });
  }
}

_bottombaritem(IconData icon, String label) {
  return BottomNavigationBarItem(icon: Icon(icon), label: label);
}
