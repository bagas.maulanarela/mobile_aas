import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class InformasiPage extends StatelessWidget {
  const InformasiPage({super.key});

  void logOut() async{
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Informasi Pencemaran Air",
        ),
      ),
      body: 
      Container(
        color: Color.fromARGB(255, 87, 162, 243),
        width: double.infinity,
        height: double.infinity,
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/pencemaran.jpeg',
              width: 300,
              height: 300,
              ),
            Text(
              "Pencemaran air adalah masalah lingkungan yang terjadi ketika bahan kimia, limbah, atau partikel lainnya mencemari air, baik sungai, danau, laut, atau sumber air lainnya. Pencemaran air dapat menyebabkan kerusakan lingkungan yang signifikan dan juga mempengaruhi kesehatan manusia dan hewan.",
              style: TextStyle(fontSize: 14.0),
              textAlign: TextAlign.justify,
            ),
            Text(
              "Dampak dari pencemaran air dapat sangat merugikan lingkungan dan kesehatan manusia. Dalam jangka pendek, pencemaran air dapat menyebabkan keracunan dan infeksi pada manusia dan hewan. Dalam jangka panjang, pencemaran air dapat mengakibatkan kerusakan sistem ekologi dan kehilangan keanekaragaman hayati.",
              style: TextStyle(fontSize: 14.0),
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ), 
    );
  }
}