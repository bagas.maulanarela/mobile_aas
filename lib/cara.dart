import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CaraPage extends StatelessWidget {
  const CaraPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Cara Menanggulangi",
        ),
      ),
      body: Container(
        color: Color.fromARGB(255, 87, 162, 243),
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/bersih.jpg',
              width: 300,
              height: 300,
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                "Berikut adalah beberapa cara untuk menanggulangi pencemaran air:",
                style:
                    GoogleFonts.montserrat(fontSize: 15.0, color: Colors.black),
                textAlign: TextAlign.justify,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                "1.Kurangi penggunaan bahan kimia\n"
                "2.Mengelola limbah dengan benar\n"
                "3.Kurangi penggunaan plastik\n"
                "4.Memperbaiki sistem sanitasi\n"
                "5.Mendorong praktik pertanian yang ramah lingkungan\n"
                "6.Mendorong penggunaan energi terbarukan\n",
                style:
                    GoogleFonts.montserrat(fontSize: 15.0, color: Colors.black),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
