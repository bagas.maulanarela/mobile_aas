import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class KadarPage extends StatelessWidget {
  const KadarPage({super.key});

  void logOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Cek Kadar Air",
        ),
      ),
      body: SafeArea(
        child: Container(
          color: Color.fromARGB(255, 87, 162, 243),
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/minum.jpg',
                width: 200,
                height: 200,
              ),
              Text(
                "Masukan Kadar Air :",
                style: TextStyle(fontSize: 15.0, color: Colors.white),
              ),
              Container(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12)),
                    hintText: "Persentase Air",
                    hintStyle: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Hasil yang diperoleh :",
                style: TextStyle(fontSize: 15.0, color: Colors.white),
              ),
              Container(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12)),
                    hintText: "",
                    hintStyle: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
