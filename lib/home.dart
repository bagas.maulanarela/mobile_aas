import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:login_flutter/about.dart';
import 'package:login_flutter/cara.dart';
import 'package:login_flutter/informasi.dart';
import 'package:login_flutter/kadar.dart';
import 'package:login_flutter/profile.dart';
import 'package:login_flutter/setting.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

void LogOut() async {
  await FirebaseAuth.instance.signOut();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    height: 130,
                    width: double.infinity,
                    color: Color.fromARGB(255, 87, 162, 243),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  alignment: Alignment.topLeft,
                                  height: 45,
                                  width: 45,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              "https://www.pngitem.com/pimgs/m/404-4042710_circle-profile-picture-png-transparent-png.png")),
                                      borderRadius: BorderRadius.circular(25),
                                      border: Border.all(
                                          color: Colors.white,
                                          style: BorderStyle.solid,
                                          width: 2)),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Halo Bagas, Selamat datang!!",
                                  style: GoogleFonts.montserrat(
                                      color: Colors.white),
                                ),
                              ],
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              child: Icon(
                                Icons.notifications_active,
                                color: Colors.white,
                                size: 26,
                              ),
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  FirebaseAuth.instance.signOut();
                                },
                                child: Icon(Icons.logout))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 1,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Container(
                          height: 60,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(30)),
                          child: TextField(
                            cursorHeight: 20,
                            autofocus: false,
                            decoration: InputDecoration(
                                hintText: "cari apa?",
                                prefixIcon: Icon(Icons.search),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: const Color.fromARGB(
                                            255, 255, 255, 255),
                                        width: 2),
                                    borderRadius: BorderRadius.circular(30))),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => KadarPage()));
                        },
                        child: Container(
                          width: 70,
                          height: 60,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/water.png',
                                width: 30,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Kadar Air',
                                style: GoogleFonts.montserrat(fontSize: 10),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AboutPage()));
                        },
                        child: Container(
                          width: 70,
                          height: 60,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/info.png',
                                width: 30,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'About',
                                style: GoogleFonts.montserrat(fontSize: 10),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CaraPage()));
                        },
                        child: Container(
                          width: 70,
                          height: 60,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/hand.png',
                                width: 30,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Pencegahan',
                                style: GoogleFonts.montserrat(fontSize: 10),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => InformasiPage()));
                        },
                        child: Container(
                          width: 70,
                          height: 60,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/writer.png',
                                width: 30,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Informasi',
                                style: GoogleFonts.montserrat(fontSize: 10),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  "Tentang Pencemaran ☘️",
                  style: GoogleFonts.montserrat(
                      fontSize: 17, fontWeight: FontWeight.bold),
                ),
              ),
              sampah(
                imagePath: "assets/images/konten_1.jpg",
                judul: "Penumpukan Sampah",
                kutipan: "Jangan Lupa Untuk Jaga Kesehatan",
              ),
              sampah(
                imagePath: "assets/images/konten_2.jpg",
                judul: "Asap Pabrik",
                kutipan: "Jangan Lupakan Masker Anda",
              ),
              sampah(
                imagePath: "assets/images/konten_5.jpg",
                judul: "Sampah di Sungai",
                kutipan: "Buang Sampah pada tempatnya",
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class sampah extends StatelessWidget {
  final String imagePath;
  final String judul;
  final String kutipan;
  const sampah(
      {Key? key,
      required this.imagePath,
      required this.judul,
      required this.kutipan})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 240,
      child: Stack(
        children: [
          Card(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            elevation: 10,
            child: Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 150,
                  child: Image.asset(
                    imagePath,
                    fit: BoxFit.cover,
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 10,
            child: SizedBox(
              height: 70,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      judul,
                      style: GoogleFonts.montserrat(
                          fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          kutipan,
                          style: GoogleFonts.montserrat(fontSize: 12),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
