import 'package:get/get.dart';
import 'package:login_flutter/home.dart';
import 'package:login_flutter/navbar/navbar.dart';
import 'package:login_flutter/profile.dart';
import 'package:login_flutter/setting.dart';

class AppPage {
  static List<GetPage> routes = [
    GetPage(name: navbar, page: () => const Navbar()),
    GetPage(name: home, page: () => const HomePage()),
    GetPage(name: setting, page: () => const SettingPage()),
    GetPage(name: profile, page: () => const ProfilePage()),
  ];

  static getnavbar() => navbar;
  static gethome() => home;
  static getsetting() => setting;
  static getprofile() => profile;
  //
  static String navbar = '/navbar';
  static String home = '/home';
  static String setting = '/setting';
  static String profile = '/profile';
}
